package com.example.democonsume.controller;

import com.example.democonsume.clients.ClienteUniversidad;
import com.example.democonsume.dto.DatosUniversidades;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/consume")
public class ConsumoController {

    @Autowired
    ClienteUniversidad clienteUniversidad;

    @GetMapping("/test")
    public ResponseEntity<?> testConsume(){
        List<DatosUniversidades> datos = clienteUniversidad.consulta();
        return ResponseEntity.ok(datos);
    }

}
