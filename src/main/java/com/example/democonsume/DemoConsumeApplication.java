package com.example.democonsume;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class DemoConsumeApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoConsumeApplication.class, args);
    }

}
