package com.example.democonsume.clients;


import com.example.democonsume.dto.DatosUniversidades;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(name = "universidades", url = "https://www.datos.gov.co")
public interface ClienteUniversidad {

    @GetMapping("/resource/5wck-szir.json")
    List<DatosUniversidades> consulta();
}
