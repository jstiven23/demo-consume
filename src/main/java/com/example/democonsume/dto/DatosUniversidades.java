package com.example.democonsume.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DatosUniversidades {

    @JsonProperty("c_digo_de_la_instituci_n")
    private String condigoInstitucion;
    private String ies_padre;
    private String instituci_n_de_educaci_n_superior_ies;
    private String principal_oseccional;
    private String id_sector;
    private String id_caracter;
    private String c_digo_del_departamento_ies;
    private String departamento_de_domicilio_de_la_ies;
    private String c_digo_del_municipio_ies;
    private String municipio_dedomicilio_de_la_ies;
    private String c_digo_snies_delprograma;
    private String programa_acad_mico;
    private String id_nivel;
    private String id_nivel_formacion;
    private String id_metodologia;
    private String id_area;
    private String id_nucleo;
    private String n_cleo_b_sico_del_conocimiento_nbc;
    private String c_digo_del_departamento_programa;
    private String departamento_de_oferta_del_programa;
    private String c_digo_del_municipio_programa;
    private String municipio_de_oferta_del_programa;
    private String id_g_nero;
    private String a_o;
    private String semestre;
    private String matriculados_2015;

}
